﻿using System;
using System.Web.Mvc;
using ECCI_IS_Lab01_WebApp.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ECCI_IS_Lab01_WebApp.Tests.Controllers
{
    [TestClass]
    public class CursoControllerTest
    {
        [TestMethod]
        public void TestIndexNotNull()
        {
            CursoController controller = new CursoController();
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestCreateNotNull()
        {
            CursoController controller = new CursoController();
            ViewResult result = controller.Create() as ViewResult;
            Assert.IsNotNull(result);
        }


        //Integration test with view because there is a fetch with the database
        [TestMethod]
        public void TestEditView()
        {
            CursoController controller = new CursoController();
            ViewResult result = controller.Edit(2) as ViewResult;
            Assert.AreEqual("Edit", result.ViewName);
        }
    }
}
